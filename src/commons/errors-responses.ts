export const errorsResponses = {
    "NothingFoundResponse" :{ data: "No result found"},
    "CannotAuthenticateResponse": {data: "Cannot authenticate user" }
}