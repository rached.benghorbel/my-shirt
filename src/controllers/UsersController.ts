import * as express from 'express';
import Users from '../entities/users.entity';
import UserService from '../services/user-service';
import { errorsResponses } from '../commons/errors-responses';
import * as bcrypt from 'bcrypt';

export default class UsersController {
    public router = express.Router();
    private path = "/users";
    private userService: UserService;
    private saltRounds = 10;

    constructor(){
        this.initializeRoutes();
        this.userService = new UserService();
    }

    private initializeRoutes = () => {
        this.router.post(this.path+"/signin",this.addUser);
        this.router.post(this.path+"/signInUser",this.addUserQuery);
        this.router.put(this.path+"/update/:id",this.updateUser);
        this.router.get(this.path,this.usersList);
        this.router.get(this.path+"/:id",this.getUser);
        this.router.delete(this.path+'/:id',this.removeUser)
    }

    addUserQuery = (request,response) => {
        const user = <Users>(request.body);
        const userService = this.userService;
        bcrypt.hash(user.userPassword,this.saltRounds).then(function(hashedPassword){
            user.userPassword = hashedPassword;
            userService.queryBuilderCruds.create(user).then(newUser => {
                response.send(newUser);
            }).catch(function(error){
                response.status(400).send(error);
            });
        }).catch(function(error){
            response.status(400).send(error);
        })
    }

    addUser = (request,response) => {
        const user = <Users>(request.body);
        const userService = this.userService;
        bcrypt.hash(user.userPassword,this.saltRounds).then(function(hashedPassword){
            user.userPassword = hashedPassword;
            userService.repositoryCruds.create(user).then(newUser => {
                response.send(newUser);
            }).catch(function(error){
                response.status(400).send(error);
            });
        }).catch(function(error){
            response.status(400).send(error);
        })
        
    }

    updateUser = (request,response) => {
        const user = <Users>(request.body);
        const userId = <Number>(request.params.id);
        this.userService.repositoryCruds.update(user,userId).then(updatedUser => 
        {
            if(updatedUser)
            {
                response.send(updatedUser);
                return;
            }else{
                response.status(404).send(errorsResponses.NothingFoundResponse);
            }    
            
        }).catch(error => {
            response.status(400).send(error);
        });
    }

    getUser = (request, response) => {
        const userId = <Number>(request.params.id);
        this.userService.queryBuilderCruds.findById(userId).then(user => {
            if(user){
                response.send(user);
            }else{
                response.status(404).send(errorsResponses.NothingFoundResponse);
            }
        }).catch(error => {
            console.log(error)
            response.status(400).send(error);
        });
    }

    usersList = (request,response) => {
        this.userService.repositoryCruds.findAll().then(users => {
            if(users){
                response.send(users);
                return;
            }
            response.status(404).send(errorsResponses.NothingFoundResponse);
        }).catch(error => {
            response.status(400).send(error);
        });
    }

    removeUser = (request,response) => {
        const receivedId = <Number>(request.params.id); 
        this.userService.repositoryCruds.findById(receivedId).then(userFound => {
            if(<Users>(userFound) ){
                this.userService.repositoryCruds.update(userFound,receivedId,0).then(user => {
                    if(user)
                    {
                        response.send(user);
                        return;
                    }
                    response.status(404).send(errorsResponses.NothingFoundResponse);
                }).catch( error => {
                    response.status(400).send(error);
                } )
            }else{
                response.status(404).send(errorsResponses.NothingFoundResponse);
            }
            
        }).catch(error => {
            response.status(400).send(error);
        })
    }
}
