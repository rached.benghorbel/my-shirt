import * as express from 'express';
import UserService from '../services/user-service';
import * as bcrypt from 'bcrypt';
import Users from 'entities/users.entity';
import * as basicAuth from 'express-basic-auth';
import * as jwt from 'jsonwebtoken';

export default class AutheticationController {
    public router = express.Router();
    private path = 'authentication';
    private userService: UserService;
    private token;
    private authenticatedUser:Users;

    constructor(){
        this.initializeRoutes();
        this.userService = new UserService();
    }

    private initializeRoutes = () => {
        this.router.post(this.path, this.authentication)
        this.router.use( 
            basicAuth({
                authorizer: this.authentication,
                authorizeAsync: true
            }),
            (req,res,next) => {
                res.setHeader("Authorization",this.token);
                res.status(200).send(this.authenticatedUser);
                next();
            }
        );
    }

    authentication = (username, password, cb) => {
        this.userService.queryBuilderCruds.findByFields(false,{userEmail:username})
            .then( (data) => {
                if(data[0]){
                    const user = <Users><unknown>(data[0]);
                    bcrypt.compare(password,<string>(user.userPassword), (err,res) => {
                        if(err || !res) return cb(null,false);
                        if(res)
                        {
                            this.token = jwt.sign({userID: (user.userEmail+user.userPassword)}, 'app-secret-token', {expiresIn: '2h'});
                            this.authenticatedUser = user;
                            return cb(null,true);
                        } 
                    })
                }else{
                    return cb(null,false);
                }
                
            } ).catch((error) => {
                return cb(null,false);
            })
    }
}