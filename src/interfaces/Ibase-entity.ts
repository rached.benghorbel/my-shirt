import { Status } from "entities/enumerators/status";

export default interface IBaseEntity {
    pk: Number;
    created?: Date;
    updated?: Date;
    deleted?: Date;
    createdId?: Number;
    updatedId?: Number;
    deletedId?: Number;
    status?: Status;
}