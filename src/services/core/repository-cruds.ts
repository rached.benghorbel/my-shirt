import { getRepository } from "typeorm";
import IBaseEntity from "interfaces/Ibase-entity";
import { Status } from "entities/enumerators/status";

export default class RepositoryCruds {
    private repository;

    constructor(className:any) {
        this.repository = getRepository(className);
    }

    public create = async(entity:IBaseEntity):Promise<IBaseEntity> => {
        const newEntity = <IBaseEntity>(this.repository.create(entity));
        newEntity.created = new Date();
        newEntity.status = 1;
        await this.repository.save(newEntity);
        return newEntity;
    }

    public update = async(entity: IBaseEntity, id: Number, status:Status = 2):Promise<IBaseEntity> => {
        let updatedEntity = <IBaseEntity>(entity);
        updatedEntity.updated = new Date();
        updatedEntity.status = status;
        await this.repository.update(id, updatedEntity);
        return this.repository.findOne(id);
    }

    public findById = async(id:Number):Promise<IBaseEntity> => {
        return await this.repository.findOne(id);
    }

    public findAll = ():Promise<IBaseEntity[]> => {
        return this.repository.find();
    }

    public delete = async(entityToDelete:IBaseEntity):Promise<IBaseEntity> => {
        entityToDelete.deleted = new Date();
        entityToDelete.status = 0;
        await this.repository.update(entityToDelete.pk,entityToDelete);
        return entityToDelete;
    }

    public destroyEntity = async(id:Number) => {
        return await this.repository.delete(id);
    }
}