import { getManager } from 'typeorm';
import IBaseEntity from 'interfaces/Ibase-entity';

export default class QueryBuilderCruds{
    private defaultTable: string;
    private relatedEntities : string[];
    private relatedTables : string[];

    constructor(relatedElements: any){
        this.defaultTable = relatedElements.defaultTable;
        this.relatedEntities = relatedElements.relatedEntities;
    }

    private addJoinedTables = (query) => {
        this.relatedEntities.forEach(relatedEntitie => {
            query.leftJoinAndSelect(this.defaultTable+"."+relatedEntitie,relatedEntitie)
        });
        return query;
    } 

    public create = async (entity:IBaseEntity,tableToInsert = this.defaultTable) => {
            const newEntity = entity;
            newEntity.created = new Date();
            newEntity.status = 1;
            return await getManager()
                        .createQueryBuilder()
                        .insert()
                        .into(tableToInsert)
                        .values(newEntity)
                        .execute();
    }

    public findById = async(pk:Number,addRelatedEntities:Boolean = true ) => {
        let searchedQuery = await getManager()
                                .createQueryBuilder(this.defaultTable,this.defaultTable)
                                .where(this.defaultTable+".pk = :id",{id:pk});
        if(this.relatedEntities && addRelatedEntities ){
            this.addJoinedTables(searchedQuery);
        }
        
        const searchedEntity = searchedQuery.getOne();       
        return searchedEntity;
    }

    public findAll = async(addRelatedEntities:Boolean = true) => {
        let query = await getManager()
                          .createQueryBuilder(this.defaultTable,this.defaultTable);
        if(this.relatedEntities && addRelatedEntities){
            this.addJoinedTables(query);
        }
        const elementsFound = query.getMany();
        return elementsFound;
    }

    public findByFields = async(addRelatedEntitites:Boolean = true,filedsToCheck ) => {
        try{
            let query = await getManager()
                          .createQueryBuilder(this.defaultTable,this.defaultTable);

            const fieldsToCheckkeys = Object.keys(filedsToCheck);
            const filedsValues = Object.values(filedsToCheck);
            fieldsToCheckkeys.forEach( (value,key) => {
                query.where(this.defaultTable+"."+value+" = :field",{field:filedsValues[key]});
            });
            if(this.relatedEntities && addRelatedEntitites){
                this.addJoinedTables(query);
            }
            const elementsFound = query.getMany();
            return elementsFound;
        }
        catch(e){
            return (e);
        }
    }

}