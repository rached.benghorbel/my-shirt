import RepositoryCruds from "./repository-cruds";
import QueryBuilderCruds from "./query-builder-cruds";

export default class BaseCrud {

    public repositoryCruds: RepositoryCruds;
    public queryBuilderCruds: QueryBuilderCruds;
    
    constructor(relatedElements){
        this.repositoryCruds = new RepositoryCruds(relatedElements.repository);
        this.queryBuilderCruds = new QueryBuilderCruds(relatedElements);   
    }
    
}