import BaseCrud from "./core/base-crud";

export default class UserService extends BaseCrud {
    
    constructor() {
        const userRepository:String ='Users';
        const userTable:string = 'Users';    
        const relatedTables = [
            'UsersFiles'
        ]
        const relatedEntities = [
            'picture'
        ] 
        const relatedElements = {
            repository: userRepository,
            defaultTable: userTable,
            relatedEntities:relatedEntities,
            relatedTables: relatedTables
        };
        super(relatedElements);
    }
}