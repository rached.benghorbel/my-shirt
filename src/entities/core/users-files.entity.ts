import FileEntity from "./file-entity";
import { Entity, ManyToOne } from "typeorm";
import Users from "../users.entity";

@Entity()
export default class UsersFiles extends FileEntity {
    
    @ManyToOne(type => Users, user => user.picture)
    public user : Users;
}