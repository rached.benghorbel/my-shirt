import BaseEntity from "./base-entity";
import { Column } from "typeorm";

export default class FileEntity extends BaseEntity{
    @Column()
    public name?: String;
    @Column()
    public size?: Number;
    @Column()
    public extension?: String;
    @Column()
    public path?: String;
    @Column({type: 'blob'})
    public content?: String;
}