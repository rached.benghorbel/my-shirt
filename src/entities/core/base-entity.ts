import { PrimaryGeneratedColumn, Column } from "typeorm";
import IBaseEntity from "../../interfaces/Ibase-entity";
import { Status } from "../enumerators/status";

export default class BaseEntity implements IBaseEntity{
    @PrimaryGeneratedColumn()
    public pk: Number;
    @Column()
    public created?: Date;
    @Column({nullable: true})
    public updated?: Date;
    @Column({nullable: true})
    public deleted?: Date;
    @Column({name : "created_id", nullable: true})
    public createdId?: Number;
    @Column({name : "updated_id", nullable: true})
    public updatedId?: Number;
    @Column({name : "deleted_id", nullable: true})
    public deletedId?: Number;
    @Column()
    public status?: Status;
}