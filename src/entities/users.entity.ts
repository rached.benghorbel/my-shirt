import UsersFiles from './core/users-files.entity';
import { Entity, Column, OneToMany, JoinTable } from 'typeorm';
import BaseEntity from './core/base-entity';

@Entity()
export default class Users extends BaseEntity{
    
    @Column({name:"user_name", type : 'varchar'})
    public userName?: string;
    
    @Column({name:"user_email", type:'varchar'})
    public userEmail?: string;
    
    @Column({name:"user_password", type:'varchar'})
    public userPassword?: string;

    @Column({name:"user_adress", type:'text', nullable: true})
    public userAdress?: string| null;    

    @OneToMany(
        type => UsersFiles, 
        photo => photo.user, 
        {
            eager: true,
            cascade: true
        }
    )
    @JoinTable()
    public picture: UsersFiles[];
    
}