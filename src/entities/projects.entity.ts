import BaseEntity from './core/base-entity';
import { Entity, Column } from 'typeorm';

@Entity()
export default class ProjectsEntity extends BaseEntity  {

    @Column( { name : 'project-name', type: "varchar" } )
    projectName: string;

    @Column( {name: "project-duration"} )
    projectDuration : number;

    @Column( { name : "project-cost" } )
    projectCost: number;

    
}