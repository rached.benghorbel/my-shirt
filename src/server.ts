import App from './app';
import UsersController from './controllers/UsersController';
import "reflect-metadata";
import config from './config/orm-config';
import { createConnection } from 'typeorm';
import AutheticationController from './controllers/AuthenticationController';

(async () => {
  try {
    await createConnection(config);
  } catch (error) {
    console.log('Error while connecting to the database', error);
    return error;
  }
  const app = new App(
    [
      new UsersController(),
      new AutheticationController()
    ],
    4000
  );
  app.listen();
})();