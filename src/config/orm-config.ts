import {ConnectionOptions } from 'typeorm';
import DatabaseConfig from './database-config';

const connection: ConnectionOptions = {
    type: 'mysql',
    host: DatabaseConfig.databaseHost,
    port: DatabaseConfig.databasePort,
    username: DatabaseConfig.databaseUser,
    password: DatabaseConfig.databasePassword,
    database: DatabaseConfig.databaseName,
    entities: [
        __dirname + '/../**/*.entity{.ts,.js}',
    ],
    synchronize: true,
};
export default connection;