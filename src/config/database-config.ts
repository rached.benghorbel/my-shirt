const DatabaseConfig = {
    databaseType : "mysql",
    databasePort : 3306,
    databaseHost : "localhost",
    databaseUser : "root",
    databasePassword : "",
    databaseName : "shirt-db"
}
export default DatabaseConfig;