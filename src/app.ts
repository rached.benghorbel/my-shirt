import * as express from 'express';
import * as bodyParser from 'body-parser';
import * as expressJwt from "express-jwt";
import * as cors from 'cors';

class App {
    public app: express.Application;
    public port: number;
    private authorizedRoutes = [
        "/authentication",
        "/users/signin",
    ];
    private corsOptions = {
        origin(origin: string, callback: (arg0: any, arg1: boolean) => void) {
            const originsWhitelist = [
                "http://localhost:3000",      // this is my front-end url for development
            ];
            const isWhitelisted = originsWhitelist.indexOf(origin) !== -1;
            callback(null, isWhitelisted);
        },
        exposedHeaders: ["Authorization"],
        credentials: true
    };

    constructor(controllers,port){
        this.app = express();
        this.port = port;
        this.initializeMiddlewares();
        this.initializeControllers(controllers);
    }

    private initializeMiddlewares = () => {
        this.app.use(cors(this.corsOptions));
        this.app.use(bodyParser.json());
        this.app.use(expressJwt({secret: "app-secret-token"}).unless({path : this.authorizedRoutes }));
    }

    private initializeControllers = (controllers) => {
        controllers.forEach((controller) => {
            this.app.use('/',controller.router);
        });
    }

    public listen = () => {
        this.app.listen(this.port);
    }
}
export default App;